import random

""" Generate Data for Testing"""


def get_state():
    """select state for testing & generate string"""
    selected_state = random.choice(['texas', 'california', 'colorado', 'florida', 'georgia', 'illinois', 'maryland',
                                    'newhampshire', 'utah', 'virginia', 'washington'])
    rs = 'state-' + selected_state + '-button'
    print(rs)
    return rs


def generate_time():
    am_pm = random.choice([' AM', ' PM'])
    if am_pm == ' AM':
        hour = random.choice(['07', '08', '09', '10', '11'])
    else:
        hour = random.choice(['12', '01', '02', '03', '04', '05', '06'])
    minutes = random.choice(['00', '15', '30', '45'])
    rt = hour + ':' + minutes + am_pm
    print('return time = ' + rt)
    return rt


def get_new_client_data():
    client_data = {
        'first_name': "John",
        'last_name': "Odd",
        'email': "john@odd.com",
        'birthday': "01/01/1999",
        'phone': "(201) 555-5555",
        'sex': "//option[. = 'Male']",
        'gender': "//option[. = 'Other']",
        'pronouns': "None"
    }
    return client_data


def get_address_info():
    client_address = {
        'address': '321 main',
        'apt': '101',
        'city': 'thisCity',
        'zip': '12345'
    }
    return client_address
