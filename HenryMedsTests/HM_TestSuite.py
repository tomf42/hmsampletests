import unittest

import HM_UI_Tests
import HM_API_Tests


def hm_test_suite_all():
    suite = unittest.TestSuite()
    suite.addTest((HM_API_Tests.Test_HM_API('test_api_test1')))
    suite.addTest(HM_UI_Tests.Test_select_state_appointment_time('test_test1_select_state'))
    suite.addTest(HM_UI_Tests.Test_select_state_appointment_time('test_test2_select_appointment'))
    suite.addTest(HM_UI_Tests.Test_select_state_appointment_time('test_test3_enter_client_data'))
    suite.addTest(HM_UI_Tests.Test_select_state_appointment_time('test_test4_enter_address'))
    return suite


def hm_test_suite_single():
    suite = unittest.TestSuite()
    suite.addTest(HM_UI_Tests.Test_select_state_appointment_time('test_test4_enter_address'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    # runner.run(hm_test_suite_single())
    runner.run(hm_test_suite_all())
