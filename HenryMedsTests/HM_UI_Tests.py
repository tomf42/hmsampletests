import time
import unittest
import random

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import HMData
import HMTestConfig


class Test_select_state_appointment_time(unittest.TestCase):
    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}

    def teardown_method(self, method):
        self.driver.quit()

    def test_test1_select_state(self):
        # Test name: test1
        self.driver = webdriver.Chrome()
        self.vars = {}
        # Step # | name | target | value
        # 1 | open | / |
        self.driver.get(HMTestConfig.site_to_test)
        # 2 | setWindowSize | 1890x2030 |
        self.driver.set_window_size(1890, 2030)
        # 3 | click | id=state-texas-button |
        self.driver.find_element(By.ID, HMData.get_state()).click()
        print(self.driver.title)
        time.sleep(3)
        "assert next available time header "
        self.assertEqual(self.driver.title, "Phentermine Appointment - HenryMeds")

    def test_test2_select_appointment(self):
        "setup env by executing test 1"
        self.test_test1_select_state()
        time.sleep(2)
        print(self.driver.title)
        self.driver.find_element(By.LINK_TEXT, HMData.generate_time()).click()
        # 5 | click | id=next-button |
        "continue to next page"
        time.sleep(3)
        self.driver.find_element(By.ID, "next-button").click()
        "verify on "
        print(self.driver.title)
        self.assertEqual(self.driver.title, "Phentermine Checkout - HenryMeds")
        time.sleep(1)

    def test_test3_enter_client_data(self):
        "setup env by executing test2"
        self.test_test2_select_appointment()
        "enter client data"
        cd = HMData.get_new_client_data()
        # 6 | click | css=#contact-step > .ex1-fieldset |
        self.driver.find_element(By.CSS_SELECTOR, "#contact-step > .ex1-fieldset").click()
        # 7 | click | name=first_name |
        self.driver.find_element(By.NAME, "first_name").click()
        # 8 | type | name=first_name | John
        self.driver.find_element(By.NAME, "first_name").send_keys(cd['first_name'])
        # 9 | click | name=last_name |
        self.driver.find_element(By.NAME, "last_name").click()
        # 10 | type | name=last_name | Odd
        self.driver.find_element(By.NAME, "last_name").send_keys(cd['last_name'])
        # 11 | click | name=email |
        self.driver.find_element(By.NAME, "email").click()
        # 12 | type | name=email | john@odd.com
        self.driver.find_element(By.NAME, "email").send_keys(cd['email'])
        # 13 | click | id=contact-form-date |
        self.driver.find_element(By.ID, "contact-form-date").click()
        # 14 | type | id=contact-form-date | 01/01/1999
        self.driver.find_element(By.ID, "contact-form-date").send_keys(cd['birthday'])
        # 15 | click | name=phone |
        self.driver.find_element(By.NAME, "phone").click()
        # 16 | type | name=phone | (201) 555-5555
        self.driver.find_element(By.NAME, "phone").send_keys(cd['phone'])
        # 17 | click | name=sex |
        self.driver.find_element(By.NAME, "sex").click()
        # 18 | select | name=sex | label=Male
        dropdown = self.driver.find_element(By.NAME, "sex")
        dropdown.find_element(By.XPATH, cd['sex']).click()
        # 19 | click | name=gender |
        self.driver.find_element(By.NAME, "gender").click()
        # 20 | select | name=gender | label=Other
        dropdown = self.driver.find_element(By.NAME, "gender")
        dropdown.find_element(By.XPATH, cd['gender']).click()
        # 21 | click | id=genderPronouns |
        self.driver.find_element(By.ID, "genderPronouns").click()
        # 22 | type | id=genderPronouns | None
        self.driver.find_element(By.ID, "genderPronouns").send_keys(cd['pronouns'])
        time.sleep(5)
        # 23 | click | id=next-button |
        self.driver.find_element(By.ID, "next-button").click()
        print(self.driver.title)
        self.assertEqual(2,2)
        time.sleep(3)

    def test_test4_enter_address(self):
        "setup env for test"
        self.test_test3_enter_client_data()
        "enter address information"
        ca = HMData.get_address_info()
        # 26 | type | name=line1 | 321 main
        self.driver.find_element(By.NAME, "line1").send_keys(ca['address'])
        # 27 | click | name=line2 |
        self.driver.find_element(By.NAME, "line2").click()
        # 28 | type | name=line2 | 101
        self.driver.find_element(By.NAME, "line2").send_keys(ca['apt'])
        # 29 | click | name=city |
        self.driver.find_element(By.NAME, "city").click()
        # 30 | type | name=city | herecity
        self.driver.find_element(By.NAME, "city").send_keys(ca['city'])
        # 31 | click | name=zip |
        self.driver.find_element(By.NAME, "zip").click()
        # 32 | type | name=zip | 12345
        self.driver.find_element(By.NAME, "zip").send_keys(ca['zip'])
        # 33 | click | id=next-button |
        self.driver.find_element(By.ID, "next-button").click()
        print(self.driver.title)
        time.sleep(3)



